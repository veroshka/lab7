#pragma once
namespace ver
{
    void Read(int& matrixSize, int mas[100][100]);

    int maximum(int matrixSize, int mas[100][100]);

    int min(int matrixSize, int mas[100][100]);
    
    bool isEightInThisElement(int a);

    bool isEightInThisRow(int i, int rowSize, int mas[100][100]);

    void SortStrok(int i, int matrixSize, int matrix[100][100]);

    void Write(int matrixSize, int mas[100][100]);
}
