﻿#include <iostream>
#include "functions.hpp"

int main()
{
    int matrixSize;
    int mas[100][100];
    ver::Read(matrixSize, mas);
    if ((ver::maximum(matrixSize, mas)) + (ver::min(matrixSize, mas)) == 0)
    {
        for (int i = 0; i < matrixSize; i++)
        {
            if (ver::isEightInThisRow(i, matrixSize, mas))
            {
                ver::SortStrok(i, matrixSize, mas);
            }
        }
    }
    ver::Write(matrixSize, mas);
}


/*Дана целочисленная матрица {Aij}i=1...n;j=1..n , n<=100. 

Если сумма наибольшего и наименьшего элементов матрицы равна нулю, 
упорядочить элементы строк, 
в которых есть хотя бы один элемент, 
содержащий цифру 8, 
по неубыванию.
*/