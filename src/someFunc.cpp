﻿#include "functions.hpp"
#include <iostream>
namespace ver
{
    void Read(int& matrixSize, int mas[100][100])
    {
        std::cin >> matrixSize;
        for (int i = 0; i < matrixSize; i++)
            for (int j = 0; j < matrixSize; j++)
                std::cin >> mas[i][j];
    }


    int maximum(int matrixSize, int mas[100][100])
    {
        int max = mas[0][0];
        for (int i = 0; i < matrixSize; i++)
            for (int j = 0; j < matrixSize; j++)
                if (mas[i][j] > max)
                {
                    max = mas[i][j];
                }

        return max;
    }

    int min(int matrixSize, int mas[100][100])
    {
        int min = mas[0][0];
        for (int i = 0; i < matrixSize; i++)
            for (int j = 0; j < matrixSize; j++)
                if (mas[i][j] < min)
                {
                    min = mas[i][j];
                }

        return min;
    }

    bool isEightInThisElement(int a)
    {
        while (a > 0)
        {
            if (a % 10 == 8)
            {
                return true;
            }
            else
            {
                a /= 10;
            }
        }
        return false;
    }

    bool isEightInThisRow(int i, int rowSize, int mas[100][100])
    {
        for (int k = 0; k < rowSize; k++)
            if (isEightInThisElement(mas[i][k]))
            {
                return true;
            }

        return false;
    }


    void SortStrok(int i, int matrixSize, int matrix[100][100])
    {
        for (int j = 0; j < matrixSize - 1; ++j)
        {
            for (int k = j + 1; k < matrixSize; ++k)
            {
                if (matrix[i][j] >= matrix[i][k])
                {
                    std::swap(matrix[i][j], matrix[i][k]);
                }
            }
        }

    }

    void Write(int matrixSize, int mas[100][100])
    {
        for (int i = 0; i < matrixSize; i++)
            for (int j = 0; j < matrixSize; j++)
                std::cout << mas[i][j] << " ";
    }
}
